!SLIDE smbullets
# Agenda #

* **Shell Basics**
* Input and Output
* Customizing Your Prompt
* Your Own Shortcuts
* Sharing Your Config
