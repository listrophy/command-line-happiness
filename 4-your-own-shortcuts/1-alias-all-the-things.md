!SLIDE smbullets
# Alias ALL THE THINGS #

* `alias ll='ls -lbF'`
* `alias la='ls -abF'`
* `alias ..='cd ..'`
* `alias ::='cd ../..'`
* `alias gap='git add -p'`
* `alias gc='git commit'`
* `alias ducks='du -cks * | sort -rn | head'`
