!SLIDE bullets
# Tab Complete is Your Friend #

* `$ brew install bash-completion`
* `$ cd ~/de<TAB>`
* `$ scp me@remote-server:~/de<TAB>`
