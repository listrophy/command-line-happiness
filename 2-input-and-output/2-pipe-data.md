!SLIDE smbullets
# Pipe Data #

* Use the pipe character
* Map, Filter, and Send

<code><pre>$ history |
  sed -E 's/^ *[0-9]+ *//' |
  sort | uniq | less</pre></code>
