!SLIDE smbullets
# Redirect with Files #

* `$ gzip < regular_file.txt > zipped_file.gz`
* `$ echo '.DS_Store' >> .gitignore`
* `$ pushd /usr/bin 1> /dev/null`
