!SLIDE
# Know Your Status #

<code><pre>$ function my\_prompt {
  modified=$(
    git status --short |
    grep '^.M'
  )
  PS1="$(
    [[ $modified != '' ]] &&
    echo '~'
  ) "
}
$ PROMPT\_COMMAND=my\_prompt
(master +~) $ ...</code></pre>
