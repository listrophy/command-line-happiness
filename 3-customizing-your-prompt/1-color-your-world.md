!SLIDE bullets
# Color Your World #

* `$ PS1="\[\e[0;31m\]$\[\e[0m\] "`
* <span style="color: red">`$`</span> ` PROMPT_COMMAND=some_function`
* `$ PS1="🚀"`
