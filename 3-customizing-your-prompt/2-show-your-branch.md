!SLIDE
# Show Your Branch #

<code><pre>$ function my\_prompt {
    PS1="$(git branch --no-color) "
  }
$ PROMPT\_COMMAND=my\_prompt</code></pre>
